import React from 'react'
import ClassCard from '../../card/ClassCard'
import PropTypes from 'prop-types'
import './RelevantClass.scss'

const garaData = {
    id: '5d2fc876109428469b6c94b0',
    thumbnail: [
        {
            url: '/uploads/ade37e367a9f4f6eaca9c2b46685a916.jpg'
        }
    ],
    title: '리비한 햇빛우산 선크림',
    owner: {
        username: '김재근'
    },
    description: '리비한 햇빛우산 선크림',
    hashtags: [
        {
            tag: '해시1'
        },
        {
            tag: '영상 강의'
        },
        {
            tag: 'E북 강의'
        }
    ]
}

const RelevantClass = props => (
    <div className='RelevantClassWrapper'>
        <div className='relevant-title'>인기 많은 연관 CLASS</div>
        <div className='relevant-grid'>
            <ClassCard classInfo={garaData} />
            <ClassCard classInfo={garaData} />
            <ClassCard classInfo={garaData} />
        </div>
    </div>
)

RelevantClass.propTypes = {
    // bla: PropTypes.string,
}

RelevantClass.defaultProps = {
    // bla: 'test',
}

export default RelevantClass
