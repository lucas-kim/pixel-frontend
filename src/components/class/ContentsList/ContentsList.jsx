import React from 'react'
import PropTypes from 'prop-types'
import { secondsToTime } from '../../../lib/common'
import './ContentsList.scss'

const garaData = [
    { title: '강의 파트 1', time: 320 },
    { title: '강의 파트 2', time: 411 },
    { title: '강의 파트 3', time: 734 }
]

const Content = ({ title, time }) => {
    return (
        <div className="content-container">
            <div className="title">{title}</div>
            <div className="time">{secondsToTime(time)}</div>
        </div>
    )
}

const ContentsList = props => (
    <div className="ContentsListWrapper">
        <div className="content-list">강의 목차</div>
        <hr />
        {garaData.map(contentData => (
            <Content title={contentData.title} time={contentData.time}></Content>
        ))}
    </div>
)

ContentsList.propTypes = {
    // bla: PropTypes.string,
}

ContentsList.defaultProps = {
    // bla: 'test',
}

export default ContentsList
