import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
// import ButtonSticky from '../common/ButtomSticky'
import './Layout.scss'
import textIcon from '../../../assets/image/layout/logoText.png'
import menuIcon from '../../../assets/image/layout/menu.png'
import kakaoIcon from '../../../assets/image/layout/kakao.png'
import facebookIcon from '../../../assets/image/layout/facebook.png'
import instagramIcon from '../../../assets/image/layout/instagram.png'
import twitterIcon from '../../../assets/image/layout/twitter.png'
import youtubeIcon from '../../../assets/image/layout/youtube.png'
import searchIcon from '../../../assets/image/layout/search-icon.png'
import searchIconBlack from '../../../assets/image/layout/search-icon-black.png'
import textLogoWhite from '../../../assets/image/layout/text-logo-white.png'
import Menu from '../common/Menu'
import ReactModal from 'react-modal'
import { useSelector, useDispatch } from 'react-redux'
import SET_USER_NAME from '../../modules/user'

const propTypes = {
    children: PropTypes.element
}

const Layout = ({ children }) => {
    const [modal, setModal] = useState(false)
    const { horizontal, theme } = useSelector(state => state.layout)

    /** Create redux state */
    const { username } = useSelector(state => state.user)

    /** Create Dispatcher */
    const dispatcher = useDispatch()
    const setUsername = username => {
        dispatcher({ type: SET_USER_NAME, payload: username })
    }

    return (
        <div className='layout'>
            <div className={`content ${!horizontal && 'none'} ${theme}`}>
                {children}
            </div>
            <Header modal={modal} setModal={setModal} />
            <Footer />
            <MenuModal modal={modal} setModal={setModal} username={username} />
        </div>
    )
}

const MenuModal = ({ modal, setModal, username }) => (
    <ReactModal
        className='Modal'
        overlayClassName='Overlay'
        isOpen={modal}
        onRequestClose={() => setModal(!modal)}
    >
        <Menu setModal={setModal} modal={modal} />
        {username}
    </ReactModal>
)

const Header = ({ modal, setModal }) => {
    const { horizontal, theme } = useSelector(state => state.layout)
    return (
        <header className={`main-header ${theme}`}>
            <div className={`header-wrapper`}>
                <div className='left'>
                    <Link href='/'>
                        <img
                            className='text-logo'
                            src={theme === 'default' ? textIcon : textLogoWhite}
                            alt='text-logo'
                            // onClick={() => {
                            //     console.log(username)
                            //     setUsername('1111111111111111')
                            // }}
                        />
                    </Link>
                    <div className={`input ${theme}`}>
                        <div className={`input-area ${theme}`} />
                        <img
                            src={
                                theme === 'default'
                                    ? searchIcon
                                    : searchIconBlack
                            }
                        />
                    </div>
                </div>
                <div className={`right ${theme}`}>
                    <div className='login'>Login</div>
                    <div className='register'>Register</div>
                    <img
                        className='menu'
                        src={menuIcon}
                        alt='user-icon'
                        onClick={() => setModal(!modal)}
                    />
                </div>
            </div>
            <div className={`header-horizontal ${!horizontal && 'none'}`}>
                {horizontal && (
                    <div className='header-horizontal-wrapper'>
                        <div className='menu-item active'>홈</div>
                        <div className='menu-item'>Class</div>
                        <div className='menu-item'>Tip</div>
                        <div className='menu-item'>Teacher</div>
                        <div className='menu-item'>카테고리</div>
                        <div className='menu-item'>신규</div>
                        <div className='menu-item'>정보</div>
                    </div>
                )}
            </div>
        </header>
    )
}

const Footer = () => {
    return (
        <footer>
            <div className='footer-template'>
                <div className='left'>
                    <div className='ft-customer-service'>
                        고객센터 <span>평일 10:00 - 19:00</span>
                    </div>
                    <div className='ft-tel'>1599-9999</div>
                    <div className='ft-email'>help@pixel.sc</div>
                    <div className='ft-sns'>
                        SNS <span>PIXEL SNS</span>
                    </div>
                    <div className='ft-sns-icons'>
                        <img
                            className='ft-sns-icon'
                            src={instagramIcon}
                            alt='instagram'
                        />
                        <img
                            className='ft-sns-icon'
                            src={twitterIcon}
                            alt='twitter'
                        />
                        <img
                            className='ft-sns-icon'
                            src={youtubeIcon}
                            alt='youtube'
                        />
                        <img
                            className='ft-sns-icon'
                            src={facebookIcon}
                            alt='facebook'
                        />
                        <img
                            className='ft-sns-icon'
                            src={kakaoIcon}
                            alt='kakao'
                        />
                        <span>카카오톡 플러스 친구</span>로 더 빠른 상담하기
                    </div>
                </div>
                <div className='right'>
                    <div className='footer'>
                        <div className='ft-tos'>
                            이용약관 | 개인정보취급방침 | 사업자확인
                        </div>
                        <div className='ft-info'>
                            상호:더앤트 사이트명:픽셀(PIXEL) 대표:이용찬
                            사업자등록번호: 120-49-23933
                        </div>
                        <div className='ft-info'>
                            통신판매:제2019-서울광진-04234
                            직업정보제공:k1390000450033
                        </div>
                        <div className='ft-info'>
                            서울특별시 광진구 능동로 243-1, 2층 픽셀(군자동)
                        </div>
                        <div className='ft-logo'>
                            <img src={textLogoWhite} alt='textLogoWhite' />
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

Layout.propTypes = propTypes

export default Layout
