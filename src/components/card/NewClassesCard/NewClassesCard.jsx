import './NewClassesCard.scss'
import SimpleClassCard from '../SimpleClassCard'
import SimpleTipCard from '../SimpleTipCard'
import classIcon from '../../../../assets/image/card/class-icon.png'
import tipIcon from '../../../../assets/image/card/tip-icon.png'

const garaData = {
    id: '5d2fc876109428469b6c94b0',
    thumbnail: [
        {
            url: '/uploads/ade37e367a9f4f6eaca9c2b46685a916.jpg'
        }
    ],
    title: '컨텐츠 제목',
    owner: {
        username: '김재근'
    },
    description: '리비한 햇빛우산 선크림',
    hashtags: [
        {
            tag: '해시1'
        },
        {
            tag: '영상 강의'
        },
        {
            tag: 'E북 강의'
        }
    ]
}

const garaData2 = {
    id: '5d2fc876109428469b6c94b0',
    thumbnail: [
        {
            url: '/uploads/72775da400944e3ca9167efe39bb6587.jpg'
        }
    ],
    title: '컨텐츠 제목',
    owner: {
        username: '김재근'
    },
    description: '리비한 햇빛우산 선크림',
    hashtags: [
        {
            tag: '해시1'
        },
        {
            tag: '영상 강의'
        },
        {
            tag: 'E북 강의'
        }
    ]
}

const NewClassesCard = () => {
    return (
        <div className='new-classes-card-template'>
            <div className='header'>NEW</div>
            <div className='ncc-content'>
                <div>
                    <div className='ncc-content-title'>
                        <img
                            className='ncc-title-icon'
                            src={classIcon}
                            alt='title-icon'
                        />
                        <div className='ncc-content-title-text'>CLASS</div>
                    </div>
                    <div className='ncc-content-grid'>
                        <SimpleClassCard classInfo={garaData} />
                        <SimpleClassCard classInfo={garaData2} />
                        <SimpleClassCard classInfo={garaData} />
                        <SimpleClassCard classInfo={garaData2} />
                        <SimpleClassCard classInfo={garaData} />
                    </div>
                </div>
                <div>
                    <div className='ncc-content-title'>
                        <img
                            className='ncc-title-icon'
                            src={tipIcon}
                            alt='title-icon'
                        />
                        <div className='ncc-content-title-text'>TIP</div>
                    </div>
                    <div className='ncc-content-grid'>
                        <SimpleTipCard classInfo={garaData2} />
                        <SimpleTipCard classInfo={garaData} />
                        <SimpleTipCard classInfo={garaData2} />
                        <SimpleTipCard classInfo={garaData} />
                        <SimpleTipCard classInfo={garaData2} />
                    </div>
                </div>
            </div>
            <div className='more-btn'>more</div>
        </div>
    )
}

export default NewClassesCard
