import ClassCard from '../ClassCard'
import { Row, Col } from 'antd'
import './PlanCard.scss'

const garaData = {
    id: '5d2fc876109428469b6c94b0',
    thumbnail: [
        {
            url: '/uploads/6b0035e68b4a4cda9075bda1dc4f19fd.PNG'
        }
    ],
    title: '리비한 햇빛우산 선크림',
    owner: {
        username: '김재근'
    },
    description: '리비한 햇빛우산 선크림',
    hashtags: [
        {
            tag: '해시1'
        },
        {
            tag: '영상 강의'
        },
        {
            tag: 'E북 강의'
        }
    ]
}

/**
 * PlanCard Props
 *  - classes
 */
const PlanCard = () => {
    return (
        <div className="paln-card-template">
            <h2>픽셀 선정 CLASS</h2>
            <Row gutter={16}>
                <Col span={12}>
                    <ClassCard classInfo={garaData} key={1} />
                </Col>
                <Col span={12}>
                    <ClassCard classInfo={garaData} key={2} />
                </Col>
            </Row>
        </div>
    )
}

export default PlanCard
