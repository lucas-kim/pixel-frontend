import PropTypes from 'prop-types'
import { backendHost } from '../../../lib/common'
import userIcon from '../../../../assets/image/bottomSticky/user.png'
import './OwnerCard.scss'

const propTypes = {
    owner: PropTypes.shape({
        username: PropTypes.string.isRequired,
        profileImage: PropTypes.shape({
            url: PropTypes.string.isRequired
        })
    }).isRequired
}

const OwnerCard = ({ owner }) => {
    const {
        username,
        profileImage: { url }
    } = owner
    return (
        <div className="owner-card-template">
            <div className="f1">
                <img src={backendHost + url} alt="profileImage" />
            </div>
            <div className="f2">
                <div className="username">{username}</div>
                <div className="career">커리어 및 경력 사항</div>
                <div className="description">강사의자기소개가 표시됩니다.</div>
            </div>
            <div className="f3">
                <img src={userIcon} atl="user" />
            </div>
        </div>
    )
}

OwnerCard.propTypes = propTypes

export default OwnerCard
