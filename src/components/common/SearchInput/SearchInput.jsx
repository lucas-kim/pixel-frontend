import { Input } from 'antd'
import searchIcon from '../../../../assets/image/search/search.png'
import './SearchInput.scss'

const SearchInput = () => {
  return (
    <div className='search-input-template'>
      <div className='left'>
        <Input placeholder='검색' />
      </div>
      <div className='right'>
        <img src={searchIcon} alt='search-icon' />
      </div>
    </div>
  )
}

export default SearchInput
