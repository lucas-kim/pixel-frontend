import { useQuery } from '@apollo/react-hooks'
import { gql } from 'apollo-boost'
import Router from 'next/router'
import './BestCategory.scss'

const BestCategory = () => {
	const { loading, error, data: { hashtags } = {} } = useQuery(GET_BEST_CATEGORY)

	if (error) return 'Error Best Category'
	if (loading) return 'Error Best Category'

	return (
		<>
			<div className="best-category-template">
				<h3>인기 카테고리</h3>
				<div className="best-category-flex">
					{hashtags.map((hashtag, i) => (
						<div
							className="best-category-button"
							key={i}
							onClick={() => Router.push(`/search?tag=${hashtag.tag}`)}
						>
							{hashtag.tag}
						</div>
					))}
				</div>
			</div>
		</>
	)
}

const GET_BEST_CATEGORY = gql`
	query {
		hashtags(sort: "classes:desc", limit: 4) {
			tag
			id
		}
	}
`

export default BestCategory
