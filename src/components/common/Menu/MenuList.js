export const menuList = [
    {
        name: '알림',
        href: '/notice',
        isFunction: false
    },
    {
        name: '프로필',
        href: '/profile',
        isFunction: false
    },
    {
        name: '나의 목록',
        href: '/mypage',
        isFunction: false
    },
    {
        name: '찜한 목록',
        href: '/mypage',
        isFunction: false
    },
    {
        name: '결제 내역',
        href: '/mypage',
        isFunction: false
    },
    {
        name: '설정',
        href: '/set',
        isFunction: false
    },
    {
        name: '로그아웃',
        href: '/logout',
        isFunction: true
    }
]
